`timescale 1ns / 1ps

module alu(Opcode,
           Operand1,
           Operand2,
           Result,
           flagCarry,
           flagZero,
           flagOverflow,
           flagNegative);
    
    input [3:0]  Opcode;
    input [7:0]  Operand1, Operand2;
    reg [8:0] ResultInternal = 9'b0;
    output reg [7:0] Result = 8'b0;
    output reg  flagCarry = 1'b0, flagZero = 1'b0, flagOverflow = 1'b0, flagNegative = 1'b0;
    
    parameter  [3:0]
    ADD = 4'b0000,
    SUB = 4'b0001,
    INC = 4'b0010,
    DEC = 4'b0011,
    EQUAL = 4'b0100,
    GREATER = 4'b0101,
    R_LEFT = 4'b0110,
    R_RIGHT = 4'b0111,
    L_LEFT = 4'b1000,
    L_RIGHT = 4'b1001,
    A_RIGHT = 4'b1010,
    NOP = 4'b1011,
    NOT = 4'b1100,
    AND = 4'b1101,
    OR = 4'b1110,
    XOR = 4'b1111;
    
    always @ (Opcode or Operand1 or Operand2)
    
    begin
    case (Opcode)
        ADD: begin
            ResultInternal = Operand1 + Operand2;
            Result = ResultInternal[7:0];
            flagCarry = ResultInternal[8];
            flagOverflow = ~(Operand1[7] ^ Operand2[7]) & (Operand1[7] ^ ResultInternal[7]);
            flagNegative = ResultInternal[7];
        end
        SUB: begin
            ResultInternal = Operand1 - Operand2;
            Result = ResultInternal[7:0];
            flagCarry = ResultInternal[8];
            flagOverflow = (Operand1[7] ^ Operand2[7]) & (Operand1[7] ^ ResultInternal[7]);
            flagNegative = ResultInternal[7];
        end
        INC: begin
            ResultInternal = Operand1 + 8'b1;
            Result = ResultInternal[7:0];
            flagOverflow = ResultInternal[8];
            flagNegative = ResultInternal[7];
        end
        DEC: begin
            ResultInternal = Operand1 - 8'b1;
            Result = ResultInternal[7:0];
            flagOverflow = ResultInternal[8];
            flagNegative = ResultInternal[7];
        end
        EQUAL: begin
            Result = (Operand1 == Operand2) ? 8'b1 : 8'b0;
        end
        GREATER: begin
            Result = (Operand1 > Operand2) ? 8'b1 : 8'b0;
        end
        R_LEFT: begin
            Result = {Operand1[6:0], Operand1[7]};
            flagNegative = ResultInternal[7];
        end
        R_RIGHT: begin
            Result = {Operand1[0], Operand1[7:1]};
            flagNegative = ResultInternal[7];
        end
        L_LEFT: begin
            ResultInternal = Operand1 << 1;
            Result = ResultInternal[7:0];
            flagCarry = ResultInternal[8];
            flagNegative = ResultInternal[7];
        end
        L_RIGHT: begin
            ResultInternal = Operand1 >> 1;
            Result = ResultInternal[7:0];
            flagCarry = ResultInternal[8];
            flagNegative = ResultInternal[7];
        end
        A_RIGHT: begin
            ResultInternal = Operand1 >>> 1;
            Result = ResultInternal[7:0];
            flagCarry = ResultInternal[8];
            flagNegative = ResultInternal[7];
        end
        NOP: begin
            
        end
        NOT: begin
            Result = ~Operand1;
            flagNegative = ResultInternal[7];
        end
        AND: begin
            Result = Operand1 & Operand2;
            flagNegative = ResultInternal[7];
        end
        OR:  begin
            Result = Operand1 | Operand2;
            flagNegative = ResultInternal[7];
        end
        XOR: begin
            Result = Operand1 ^ Operand2;
            flagNegative = ResultInternal[7];
        end
        default: begin
            Result = 8'b0;
            flagCarry = 1'b0;
            flagOverflow = 1'b0;
            flagNegative = 1'b0;
        end
    endcase

    flagZero = Result == 8'b0;

    end
    
endmodule
