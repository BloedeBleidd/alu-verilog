`timescale 1ns / 1ps

`include "alu.v"

module alu_tb; 
reg [3:0] Opcode;
reg [7:0] Operand1;
reg [7:0] Operand2;
wire [7:0] Result;
wire flagCarry;
wire flagZero;
wire flagOverflow;
wire flagNegative;
alu uut (.Opcode(Opcode), .Operand1(Operand1), .Operand2(Operand2), .Result(Result), .flagCarry(flagCarry), .flagZero(flagZero), .flagOverflow(flagOverflow), .flagNegative(flagNegative));

reg [4:0] count = 4'd0;
reg [8:0] a = 8'd0;
reg [8:0] b = 8'd0;

initial begin
    $dumpfile("alu_tb.vcd");
    $dumpvars;
    
    Opcode   = 4'b0;
    Operand1 = 8'b0;
    Operand2 = 8'b0;
    #20;

    for (a = 0; a < 256; a = a + 1'b1)
    begin
        for (b = 0; b < 256; b = b + 1'b1)
        begin
            for (count = 0; count < 16; count = count + 1'b1)
            begin
                Opcode = count[3:0];
                Operand1 = a[7:0];
                Operand2 = b[7:0];

                $monitor("OC:%b A:%b B:%b R:%b C:%b Z:%b O:%b N:%b", Opcode, Operand1, Operand2, Result, flagCarry, flagZero, flagOverflow, flagNegative);
                #10;
            end
        end
    end
    $finish;
end

endmodule
